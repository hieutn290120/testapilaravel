<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PathResetPass;
use Carbon\Carbon;

class CommandRecord extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'path:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        PathResetPass::where('created_at', '<=' , Carbon::now('Asia/Ho_Chi_Minh')->subMinutes(1))->delete();
        $this->info('Command description:Cron Command Run successfully!');
    }
}
