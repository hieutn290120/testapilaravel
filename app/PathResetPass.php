<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PathResetPass extends Model
{
    //
    protected $table = 'reset_path';
    public $timestamps = 'true';

    protected $fillable = ['path'];
}
